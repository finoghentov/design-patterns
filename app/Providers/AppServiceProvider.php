<?php

namespace App\Providers;

use App\DesignPatterns\Adapter\Classes\ExcelCreatorOld;
use App\DesignPatterns\Adapter\Contracts\ExcelCreatorOldContract;
use App\DesignPatterns\Helper;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        app()->bind(ExcelCreatorOldContract::class, ExcelCreatorOld::class);

        View::composer('*', function ($view){
            $view->with('links', Helper::getSideBarLinks());
        });

    }
}
