<?php

namespace App\Http\Controllers;

use App\DesignPatterns\Adapter\Classes\ExcelAdapter;
use App\DesignPatterns\Adapter\Classes\ExcelCreatorNew;
use App\DesignPatterns\Adapter\Classes\ExcelCreatorOld;
use App\DesignPatterns\Adapter\Contracts\ExcelCreatorOldContract;
use App\DesignPatterns\Builder\Classes\MysqlQueryBuilder;
use App\DesignPatterns\Builder\Models\Order;
use App\DesignPatterns\Facades\Event;
use App\DesignPatterns\Helper;
use App\DesignPatterns\Delegation\AppRequest;
use App\DesignPatterns\Delegation\Requests\FormRequest;
use App\DesignPatterns\EventChannel\Channel\EventChannel;
use App\DesignPatterns\EventChannel\Publisher\Publisher;
use App\DesignPatterns\EventChannel\Subscriber\Subscriber;
use App\DesignPatterns\InterfacePattern\RequestInterfaceClass;
use App\DesignPatterns\LazyLoad\Classes\Category;
use App\DesignPatterns\Observer\Observers\AdminObserver;
use App\DesignPatterns\Observer\Observers\UserObserver;
use App\DesignPatterns\Observer\User;
use App\DesignPatterns\PropertyContainer\ExampleClass;
use App\DesignPatterns\Singleton\Examples\Config;
use App\DesignPatterns\Singleton\Examples\Logger;
use App\DesignPatterns\Strategy\Classes\PaymentService;
use App\DesignPatterns\Strategy\Factories\PaymentFactory;
use Barryvdh\Debugbar\Facade;
use Exception;

class PatternController extends Controller
{
    public function main(){
        return view('main')->with(Helper::mainDefinition());
    }

    /**
     * PropertyContainer design pattern example
     */
    public function propertyContainerAction()
    {
        $obj = new ExampleClass();

        Facade::debug('Пустой созданный объект #1', $obj);

        $obj->fillAttributes([
            'name' => 'Виктор',
            'surname' => 'Иванов',
            'phone' => '000000',
            'address' => 'Казахстан'
        ]);

        Facade::debug('Заполненный объект', $obj);

        $obj2 = new ExampleClass([
            'name' => 'Pavel',
            'surname' => 'Finoghentov',
            'phone' => '000000',
            'address' => 'Chisinau'
        ]);

        Facade::debug('Сразу заполенный объект #2', $obj2);

        $obj2->setAttribute('new_attr', 'New value');

        Facade::debug('Изменённый объект', $obj2);

        return view('main')->with(Helper::propertyContainerDefinition());
    }

    /**
     * Delegation design pattern example
     */
    public function delegateAction()
    {
        Facade::debug('Первый пример. Есть правила и они соблюдены');

        $request = new FormRequest([ //Simulating request data
            'name' => 'Pavel',
            'email' => 'pavel@gmail.com',
            'salary' => 535
        ]);

        Facade::debug([$request, $request->rules(), $request->validate()]); // no errors

        Facade::debug('Второй пример, есть правила и они не соблюдены');

        $request = new FormRequest([
           'name' => 100,
           'salary' => 'salary'
        ]);

        Facade::debug([$request, $request->rules(), $request->validate()]); // errors

        Facade::debug('Третий пример, нет правил, попытка валидации');

        $request = new AppRequest([
            'name' => 'John'
        ]);

        try{
            $request->validate();
        }catch (Exception $exception){
            Facade::debug([$request, $request->rules(), 'Catch exception because we can\'t validate without rules']);
        }

        return view('main')->with(Helper::delegationDefinition());
    }

    /**
     * Event channel pattern example
     */
    public function eventChannel()
    {
        $channel = EventChannel::make();

        $vegetables_publisher = Publisher::make('vegetables', $channel);

        Subscriber::make('John')->subscribe('vegetables', $channel);

        Subscriber::make('Elise')->subscribe('vegetables', $channel);

        $vegetables_publisher->publish('New vegetables come');

        $fruit_publisher = Publisher::make('fruits', $channel);

        Subscriber::make('Pavel')->subscribe('fruits', $channel);

        $fruit_publisher->publish('Apples finished');

        Facade::debug([$channel, $vegetables_publisher]);

        return view('main')->with(Helper::eventChannelDefinition());
    }

    /**
     * Interface pattern example
     *
     * We have interface that RequestInterfaceClass give us for using his methods
     * We don't need to know what classes and objects RequestInterfaceClass uses
     */
    public function interface()
    {
        $request = new RequestInterfaceClass;

        Facade::debug($request->url());

        Facade::debug($request->segments());

        Facade::debug($request->segment(1));

        Facade::debug($request->segment(5));

        return view('main')->with(Helper::interfaceDefinition());
    }

    /**
     * Singleton pattern example
     */
    public function singleton()
    {
        Facade::debug('Example with abstract class extending');

        $obj1 = Logger::log('First log');

        $obj2 = Logger::log('Second log');

        try{
            new Logger;
        }catch (\Error $exception){
            $obj3 = Logger::log('Catch exception for creation of Logger class');
        }

        try{
            clone Logger::getInstance();
        }catch (\Error $exception){
            $obj4 = Logger::log('Catch exception for clone of Logger class');
        }

        try{
            serialize(Logger::getInstance());
        }catch (\ErrorException $exception){
            $obj5 = Logger::log('Catch exception for clone of Logger class');
        }

        Facade::debug('Check if all Logger instances are the same:');

        Facade::debug($obj1 === $obj2 && $obj1 === $obj3 && $obj1 === $obj3 && $obj1 === $obj4 && $obj1 === $obj5);

        Facade::debug('Check if all Config instances are the same:');

        $config1 = Config::getInstance();
        $config2 = Config::getInstance();
        $config3 = Config::getInstance();

        Facade::debug($config1 === $config2 && $config1 === $config3);

        return view('main')->with(Helper::singletonDefinition());
    }

    /**
     * Builder pattern example
     */
    public function builder()
    {
        $request = app(AppRequest::class);

        $builder = MysqlQueryBuilder::query()->table('users')->select(['id', 'name', 'phone']);

        $builder->where('id', '>', 0);

        if($request->has('status')){
            $builder->where('status', '=', $request->get('status'));
        }

        if($request->has('limit')){
            $builder->limit($request->get('limit'));
        }

        $users_query = $builder->get();

        Logger::log($users_query);

        $orders_query = Order::query()->get();

        Logger::log($orders_query);

        $orders_query = Order::query()->select(['user_id'])->get();

        Logger::log($orders_query);

        $orders_query = Order::query()->select(['user_id'])->where('finished', '=', 0)->get();

        Logger::log($orders_query);

        return view('main')->with(Helper::builderDefinition());
    }

    /**
     * LazyLoad pattern example
     */
    public function lazyLoad()
    {
        $category = new Category(['name' => 'Test Category 1']);

        Facade::debug('First initialized Category without books');
        Facade::debug($category);

        Facade::debug('Ask category for books');
        Facade::debug($category->books);

        Facade::debug('Now Category Object has books');
        Facade::debug($category);

        Facade::debug('For more clean we will work only with first book. Now Book doesn\'t has author');
        Facade::debug($category->books[0]);
        Facade::debug('Call book author');
        Facade::debug($category->books[0]->author);
        Facade::debug('Now book has author object');
        Facade::debug($category->books[0]);
        Facade::debug('And category has author object in book object');
        Facade::debug($category);

        return view('main')->with(Helper::lazyLoadDefinition());
    }

    /**
     * Strategy pattern example
     * @throws Exception
     */
    public function strategy()
    {
        $order_data = [
            'id' => rand(1, 200)
        ];

        $types = ['paypal', 'stripe', 'cc'];

        $type = $types[array_rand($types)];

        Logger::log('Current type is: '. $type);

        $payment = PaymentService::make(
            PaymentFactory::make($type)
        );

        $charge = $payment->charge($order_data);

        Logger::log($charge);

        $validation = $payment->validate($order_data, $charge);

        Logger::log($validation);

        return view('main')->with(Helper::strategyDefinition());
    }

    /**
     * Observer pattern example
     * In this example I will use session instead of real storage
     * I will change some data of observable object and it will change before pushing in storage
     * @throws Exception
     */
    public function observer()
    {
        User::observe(UserObserver::class);
        User::observe(AdminObserver::class);

        $user = new User([
            'name' => 'Pavel',
            'surname' => 'Finoghentov',
            'email' => 'caster977@gmail.com',
        ]);

        Facade::debug(session()->get('saved_items_' . $user->id));

        $user->update([
            'name' => 'Pavel',
            'surname' => 'AnotherName',
            'email' => 'caster977@gmail.com'
        ]);
        Facade::debug(session()->get('saved_items_' . $user->id));

        $user->delete();

        Facade::debug(session()->get('saved_items_' . $user->id));

        $user = new User([
            'name' => 'Boss',
            'admin' => true
        ]);
        Facade::debug(session()->get('saved_items_' . $user->id));

        $user->update(array_merge($user->getAttributes(), ['status' => 'active']));

        Facade::debug(session()->get('saved_items_' . $user->id));

        $user->delete();

        Facade::debug(session()->get('saved_items_' . $user->id));

        return view('main')->with(Helper::observerDefinition());
    }

    /**
     * Adapter design pattern
     */
    public function adapter()
    {
        // Let's image that this binding happens in Service Provider
        app()->bind(ExcelCreatorOldContract::class, ExcelCreatorOld::class);

        Facade::debug('Old realization with old interface:');

        // I will explain this step in the end. We could use just (new ExcelCreatorOld) but we could face the problem
        $old_excel = app(ExcelCreatorOldContract::class);

        $old_excel->newColumn('id');
        $old_excel->newColumn('name');
        $old_excel->newRow([2, 'Pavel']);

        $old_excel->generate();

        Facade::debug('New realization with new/different interface:');

        $new_excel = new ExcelCreatorNew;

        $new_excel->addColumn('ID')->addColumn('Name')->addColumn('Phone')->addColumn('Address');

        $new_excel->addRow([1, 'Pavel', '000', 'Chisinau'])->addRow([99, 'Daniil', '555', 'Chisinau']);

        $new_excel->build();

        Facade::debug('Lets imagine that whole our project using old interface. But we have to adapt old interface to new interface. Adapter Design Pattern will help us');

        $adapter = new ExcelAdapter;

        $adapter->newColumn('id');
        $adapter->newColumn('name');

        $adapter->newRow([2, 'Pavel']);

        $adapter->generate();

        Facade::debug('But now we face one problem. We don\'t have to change realization in whole project, but we need to change old excel creator object to Adapter class.');
        Facade::debug('Interface binding will help us here. Because we smart guys and we bind ExcelInterface with realization, we can just rebind it now');


        Facade::debug('Old realization:');
        Facade::debug(get_class(app(ExcelCreatorOldContract::class)));


        //Imagine that we change this after years in our service provider
        app()->bind(ExcelCreatorOldContract::class, ExcelAdapter::class);

        Facade::debug('Realization after binding now:');

        Facade::debug(get_class(app(ExcelCreatorOldContract::class)));

        return view('main')->with(Helper::adapterDefinition());
    }
}
