<?php


namespace App\DesignPatterns\Observer\Contracts;


interface ObservedContract
{
    /**
     * @param ObserverContract $observer
     */
    public static function observe(ObserverContract $observer): void;
}
