<?php


namespace App\DesignPatterns\Observer\Observers;


use App\DesignPatterns\Observer\Contracts\ObservedContract;
use App\DesignPatterns\Observer\Contracts\ObserverContract;
use Barryvdh\Debugbar\Facade;

class UserObserver implements ObserverContract
{
    /**
     * @param ObservedContract $item
     */
    public function created(ObservedContract $item): void
    {
        $item->id = rand(0, 100000);
        $item->status = 'new';

         Facade::debug('Staff Created');
    }

    /**
     * @param ObservedContract $item
     */
    public function updated(ObservedContract $item): void
    {
        $item->status = 'updated';
        Facade::debug('Staff updated');
    }

    /**
     * @param ObservedContract $item
     */
    public function deleted(ObservedContract $item): void
    {
        Facade::debug('Staff deleted');
    }
}
