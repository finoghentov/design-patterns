<?php


namespace App\DesignPatterns\Observer\Observers;


use App\DesignPatterns\Observer\Contracts\ObservedContract;
use App\DesignPatterns\Observer\Contracts\ObserverContract;

class AdminObserver implements ObserverContract
{
    /**
     * @param ObservedContract $item
     */
    public function created(ObservedContract $item): void
    {
        if(!$item->admin){
            \Debugbar::debug('Создан не админ');
        }else{
            \Debugbar::debug('Создан админ');
        }
    }

    /**
     * @param ObservedContract $item
     */
    public function updated(ObservedContract $item): void
    {
        if(!$item->admin){
            \Debugbar::debug('Обновлён не админ');
        }else{
            \Debugbar::debug('Обновлён админ');
        }
    }

    /**
     * @param ObservedContract $item
     */
    public function deleted(ObservedContract $item): void
    {
        if(!$item->admin){
            \Debugbar::debug('Удалён не админ');
        }else{
            \Debugbar::debug('Удалён админ');
        }
    }
}
