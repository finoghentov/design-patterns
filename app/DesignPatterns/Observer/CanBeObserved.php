<?php


namespace App\DesignPatterns\Observer;

use Exception;
use Illuminate\Support\Arr;

trait CanBeObserved
{
    /**
     * @var array
     */
    private static array $observers = [];

    /**
     * @param array|string $classes
     * @throws Exception
     */
    public static function observe($classes): void
    {
        foreach (Arr::wrap($classes) as $class){
            self::registerObserver($class);
        }
    }

    /**
     * @param $class
     * @throws Exception
     */
    public static function registerObserver($class): void
    {
        $className = self::resolveObserverClassName($class);

        self::$observers[] = $className;
    }

    /**
     * @param $class
     * @return string
     * @throws Exception
     */
    protected static function resolveObserverClassName($class): string
    {
        if(is_object($class)){
            return get_class($class);
        }

        if(class_exists($class)){
            return $class;
        }

        throw new Exception('Class not exists');
    }

    /**
     * @return array
     */
    protected function getObservers(): array
    {
        return self::$observers;
    }

    /**
     * @param string $event
     */
    protected function fireEvent(string $event): void
    {
        foreach($this->getObservers() as $observer){
            if(method_exists($observer, $event)){
                (new $observer)->{$event}($this);
            }
        }
    }
}
