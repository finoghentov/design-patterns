<?php


namespace App\DesignPatterns\EventChannel\Publisher;


interface PublisherContract
{
    /**
     * @param $data
     */
    public function publish($data);
}
