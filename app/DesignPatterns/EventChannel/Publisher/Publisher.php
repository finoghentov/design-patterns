<?php


namespace App\DesignPatterns\EventChannel\Publisher;


use App\DesignPatterns\EventChannel\Channel\EventChannelContract;

class Publisher implements PublisherContract
{
    /**
     * @var Publisher|string
     */
    private string $topic;

    /**
     * @var EventChannelContract
     */
    private EventChannelContract $channel;

    /**
     * Publisher constructor.
     *
     * @param string $topic
     * @param EventChannelContract $channel
     */
    public function __construct(string $topic, EventChannelContract $channel)
    {
        $this->topic = $topic;
        $this->channel = $channel;
    }

    /**
     * Create new instance of publisher
     *
     * @param string $topic
     * @param EventChannelContract $channel
     * @return Publisher
     */
    public static function make(string $topic, EventChannelContract $channel): Publisher
    {
        return new static($topic, $channel);
    }

    /**
     * @param $data
     */
    public function publish($data)
    {
        $this->channel->publish($this->topic, $data);
    }
}
