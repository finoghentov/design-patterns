<?php


namespace App\DesignPatterns\EventChannel\Subscriber;


use App\DesignPatterns\EventChannel\Channel\EventChannelContract;

interface SubscriberContract
{
    /**
     * @param string $topic
     * @param EventChannelContract $channel
     * @return void
     */
    public function subscribe(string $topic, EventChannelContract $channel): void;

    /**
     * @param string $topic
     * @param $data
     * @return void
     */
    public function notify(string $topic, $data): void;

    /**
     * @return string
     */
    public function getName(): string;
}
