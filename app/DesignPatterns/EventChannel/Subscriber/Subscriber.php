<?php


namespace App\DesignPatterns\EventChannel\Subscriber;


use App\DesignPatterns\EventChannel\Channel\EventChannelContract;
use Barryvdh\DebugBar\Facade;

class Subscriber implements SubscriberContract
{
    /**
     * @var string
     */
    public string $name;

    /**
     * Subscriber constructor.
     *
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * Create new instance of Subscriber
     *
     * @param string $name
     * @return Subscriber
     */
    public static function make(string $name): Subscriber
    {
        return new static($name);
    }

    /**
     * @param string $topic
     * @param EventChannelContract $channel
     * @return void
     */
    public function subscribe(string $topic, EventChannelContract $channel): void
    {
        $channel->subscribe($topic, $this);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Notification for subscriber after publishing
     *
     * @param $data
     */
    public function notify(string $topic, $data): void
    {
        \Debugbar::debug(sprintf(
            '%s was notified on %s. %s', $this->getName(), $topic, $data
        ));
    }
}
