<?php


namespace App\DesignPatterns\EventChannel\Channel;

use App\DesignPatterns\EventChannel\Subscriber\SubscriberContract;

class EventChannel implements EventChannelContract
{
    /**
     * @var array
     */
    private array $topics = [];

    /**
     * Create new instance of EventChannel
     *
     * @return EventChannel
     */
    public static function make(): EventChannel
    {
        return new static();
    }

    /**
     * @param string $topic
     * @param SubscriberContract $subscriber
     */
    public function subscribe(string $topic, SubscriberContract $subscriber): void
    {
        $this->topics[$topic][] = $subscriber;
    }

    /**
     * @param string $topic
     * @param $data
     * @return mixed
     */
    public function publish(string $topic, $data): void
    {
        if(empty($this->topics[$topic])){
            return;
        }

        foreach($this->topics[$topic] as $subscriber){
            $subscriber->notify($topic, $data);
        }
    }
}
