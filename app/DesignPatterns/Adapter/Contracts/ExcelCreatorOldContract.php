<?php


namespace App\DesignPatterns\Adapter\Contracts;


interface ExcelCreatorOldContract
{
    /**
     * @param string $title
     * @return mixed
     */
    public function newColumn(string $title);

    /**
     * @param array $data
     * @return mixed
     */
    public function newRow(array $data);


    /**
     * @return mixed
     */
    public function generate();
}
