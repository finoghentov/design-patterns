<?php


namespace App\DesignPatterns\Adapter\Classes;


use App\DesignPatterns\Adapter\Contracts\ExcelCreatorOldContract;
use Barryvdh\Debugbar\Facade;

class ExcelCreatorOld implements ExcelCreatorOldContract
{
    /**
     * @var array
     */
    public array $columns = [];

    /**
     * @var array
     */
    public array $rows = [];

    /**
     * @param string $title
     * @return mixed
     */
    public function newColumn(string $title)
    {
        $this->columns[] = $title;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function newRow(array $data)
    {
        $this->rows[] = $data;
    }

    /**
     * @return mixed
     */
    public function generate()
    {
        Facade::debug(
            array_merge([$this->columns], $this->rows)
        );
    }
}
