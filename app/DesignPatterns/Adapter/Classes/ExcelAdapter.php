<?php


namespace App\DesignPatterns\Adapter\Classes;


use App\DesignPatterns\Adapter\Contracts\ExcelCreatorOldContract;
use Barryvdh\Debugbar\Facade;

class ExcelAdapter implements ExcelCreatorOldContract
{
    /**
     * @var ExcelCreatorNew
     */
    private ExcelCreatorNew $adapter;

    public function __construct()
    {
        $this->adapter = new ExcelCreatorNew();
    }

    /**
     * @param string $title
     * @return mixed
     */
    public function newColumn(string $title)
    {
        return $this->adapter->addColumn($title);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function newRow(array $data)
    {
        return $this->adapter->addRow($data);
    }

    /**
     * @return mixed
     */
    public function generate()
    {
        $this->adapter->build();
    }
}
