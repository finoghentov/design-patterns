<?php


namespace App\DesignPatterns\Strategy\Classes;


use App\DesignPatterns\Singleton\Examples\Logger;
use App\DesignPatterns\Strategy\Contracts\PaymentStrategy;

class StripePayment implements PaymentStrategy
{
    private const SECRET = 'IAmStipeSecretKey';

    /**
     * @param $order_data
     * @return mixed
     */
    public function charge($order_data)
    {
        Logger::log('Stripe Payment Processing:');

        return [
            'stripe_hash' => md5(self::SECRET . $order_data['id'])
        ];
    }

    /**
     * @param $order_data
     * @param $validate_data
     * @return mixed
     */
    public function validate($order_data, $validate_data)
    {
        Logger::log('Stripe Payment Validation Processing:');

        return $validate_data['stripe_hash'] === md5(self::SECRET . $order_data['id']);
    }
}
