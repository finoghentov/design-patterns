<?php


namespace App\DesignPatterns\Strategy\Classes;


use App\DesignPatterns\Singleton\Examples\Logger;
use App\DesignPatterns\Strategy\Contracts\PaymentStrategy;

class CreditCardPayment implements PaymentStrategy
{
    private const SECRET = 'IAmCreditCardSecretKey';

    /**
     * @param $order_data
     * @return mixed
     */
    public function charge($order_data)
    {
        Logger::log('CreditCard Payment Processing:');

        return [
            'credit_card_hash' => md5(self::SECRET . $order_data['id'])
        ];
    }

    /**
     * @param $order_data
     * @param $validate_data
     * @return mixed
     */
    public function validate($order_data, $validate_data)
    {
        Logger::log('CreditCard Payment Validation Processing:');

        return $validate_data['credit_card_hash'] === md5(self::SECRET . $order_data['id']);
    }
}
