<?php


namespace App\DesignPatterns\Builder\Contracts;


interface SqlQueryBuilder
{
    const SELECT = 'select';
    const UPDATE = 'update';
    const INSERT = 'insert';
    const DELETE = 'delete';

    /**
     * @return mixed
     */
    public static function query();

    /**
     * @param string $table
     * @return $this
     */
    public function table(string $table): self;

    /**
     * @param array $columns
     * @return $this
     */
    public function select(array $columns): self;

    /**
     * @param string $column
     * @param string $action
     * @param $value
     * @return $this
     */
    public function where(string $column, string $action , $value): self;

    /**
     * @param int $amount
     * @return $this
     */
    public function limit(int $amount): self;

    /**
     * @return string
     */
    public function get(): string;
}
