<?php


namespace App\DesignPatterns\PropertyContainer;

/**
 * Interface PropertyContainerContract
 * @property array $attributes
 * @package App\DesignPatterns\PropertyContainer
 */
interface PropertyContainerContract
{
    /**
     * Get attribute from property container
     *
     * @param string $key
     * @return mixed
     */
    public function getAttribute(string $key);

    /**
     * Get attribute from property container
     *
     * @param string $key
     * @param $value
     * @return void
     */
    public function setAttribute(string $key, $value);

    /**
     * Fill class attributes with data
     *
     * @param array $attributes
     * @return void
     */
    public function fillAttributes(array $attributes);
}
