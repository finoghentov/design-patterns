<?php


namespace App\DesignPatterns\PropertyContainer;


abstract class AbstractPropertyContainer implements PropertyContainerContract
{
    /**
     * @var array
     */
    private array $attributes = [];

    /**
     * TestClass constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $this->fillAttributes($attributes);
    }

    /**
     * @param $key
     * @return mixed
     */
    public function __get($key)
    {
        return $this->getAttribute($key);
    }

    /**
     * @param $key
     * @param $value
     */
    public function __set($key, $value)
    {
        $this->setAttribute($key, $value);
    }

    /**
     * Fill class with attributes
     *
     * @param array $data
     * @return void
     */
    public function fillAttributes(array $data): void
    {
        $this->attributes = $data;
    }

    /**
     * Get attribute from property container
     *
     * @param $key
     * @return mixed|null
     */
    public function getAttribute(string $key)
    {
        if(property_exists(self::class, $key)){
            return $this->{$key};
        }

        return $this->attributes[$key] ?? null;
    }

    /**
     * Get all attributes from property container
     *
     * @return mixed|null
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * Set attribute to property container
     *
     * @param $key
     * @param $value
     */
    public function setAttribute($key, $value): void
    {
        if(property_exists(self::class, $key)){
            $this->{$key} = $value;
        }else{
            $this->attributes[$key] = $value;
        }
    }
}
