<?php


namespace App\DesignPatterns\Singleton\Examples;


use App\DesignPatterns\PropertyContainer\AbstractPropertyContainer;
use App\DesignPatterns\Singleton\Traits\SingletonTrait;

class Config
{
    use SingletonTrait;

    /**
     * @param string $key
     * @return string
     */
    public function getConfig(string $key): string
    {
        return 'You want to get config from ' . $key . ' key';
    }

    /**
     * @param string $key
     * @return string
     */
    public static function config(string $key): string
    {
        return self::getInstance()->getConfig($key);
    }
}
