<?php


namespace App\DesignPatterns\Singleton\Examples;


use App\DesignPatterns\Singleton\Singleton;
use Barryvdh\Debugbar\Facade;

class Logger extends Singleton
{
    /**
     * @param $data
     */
    public function writeLog($data): void
    {
        Facade::debug($data);
    }

    /**
     * Write log
     *
     * @param $data
     * @return Logger
     */
    public static function log($data)
    {
        $obj = static::getInstance();
        $obj->writeLog($data);

        /**
         * For checking comparing instances I will return current class instance
         */
        return $obj;
    }
}
