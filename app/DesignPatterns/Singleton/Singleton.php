<?php


namespace App\DesignPatterns\Singleton;


abstract class Singleton
{
    /**
     * @var array $this
     */
    public static array $instances = [];

    /**
     * Forbid for create new instance.
     */
    private function __construct()
    {
        //
    }

    /**
     * Forbid for clone new instance
     */
    private function __clone()
    {
        //
    }

    /**
     * Forbid for serialize new instance
     */
    private function __sleep()
    {
        //
    }

    /**
     * Forbid for deserialize new instance
     */
    private function __wakeup()
    {
        //
    }

    /**
     * Return singleton instance
     *
     * @return static
     */
    public static function getInstance(): self
    {
        $cls = static::class;

        if (!isset(self::$instances[$cls])) {
           self::$instances[$cls] = new static();
        }

        return self::$instances[$cls];
    }
}
