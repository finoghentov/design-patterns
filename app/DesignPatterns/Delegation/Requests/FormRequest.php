<?php


namespace App\DesignPatterns\Delegation\Requests;


use App\DesignPatterns\Delegation\AppRequest;

class FormRequest extends AppRequest
{
    /**
     * Rewriting AppRequest rules method
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'email' => ['required', 'email'],
            'name' => ['required', 'string'],
            'salary' => ['required', 'integer']
        ];
    }

    /**
     * Rewriting AppRequest messages method
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            'email' => [
                'email' => 'Test for changing validation error message'
            ],
            'salary' => [
                'required' => 'Text for salary required'
            ]
        ];
    }
}
