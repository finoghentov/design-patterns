<?php


namespace App\DesignPatterns\Delegation\Validator;


trait CanValidateAttributes
{
    /**
     * @param string $key
     * @param array $attributes
     * @return bool
     */
    public function validateRequiredRule(string $key, array $attributes): bool
    {
        return array_key_exists($key, $attributes);
    }

    /**
     * Check if value is integer
     *
     * @param string $key
     * @param $value
     * @return bool
     */
    public function validateIntegerRule(string $key, $value): bool
    {
        return filter_var($value, FILTER_VALIDATE_INT);
    }

    /**
     * Check if value is string
     *
     * @param string $key
     * @param $value
     * @return bool
     */
    public function validateStringRule(string $key, $value): bool
    {
        return is_string($value);
    }

    /**
     * Check if value is email
     *
     * @param string $key
     * @param $value
     * @return bool
     */
    public function validateEmailRule(string $key, $value): bool
    {
        return filter_var($value, FILTER_VALIDATE_EMAIL);
    }
}
