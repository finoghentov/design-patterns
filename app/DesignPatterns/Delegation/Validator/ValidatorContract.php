<?php


namespace App\DesignPatterns\Delegation\Validator;


interface ValidatorContract extends CanValidateContract
{
    /**
     * @param array $data
     * @param array $rules
     * @param array $messages
     *
     * @return ValidatorContract
     */
    public static function make(array $data, array $rules, array $messages): ValidatorContract;
}
