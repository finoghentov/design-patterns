<?php


namespace App\DesignPatterns\Delegation\Validator;


interface CanValidateContract
{
    /**
     * @return mixed
     */
    public function validate();
}
