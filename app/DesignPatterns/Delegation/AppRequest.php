<?php


namespace App\DesignPatterns\Delegation;


use App\DesignPatterns\Delegation\Validator\CanValidateContract;
use App\DesignPatterns\Delegation\Validator\Validator;

class AppRequest extends AbstractRequest implements CanValidateContract
{
    /**
     * Get rules for validator request attributes
     *
     * @return array
     */
    public function rules(): array
    {
        return [];
    }

    /**
     * Get messages for validation rules
     *
     * @return array
     */
    public function messages(): array
    {
        return [];
    }

    /**
     * Get value from attributes by key
     *
     * @param string $key
     * @return mixed|null
     */
    public function get(string $key)
    {
        return $this->getAttribute($key);
    }

    /**
     * Check if parameter in request attributes
     *
     * @param string $key
     * @return bool
     */
    public function has(string $key): bool
    {
        return (bool) $this->get($key);
    }

    /**
     * Delegate task for validating data
     * Validator class is independent and can be used without AppRequest
     * This is example of Delegation Pattern
     *
     * @return mixed
     */
    public function validate()
    {
        $validator = Validator::make(
            $this->getAttributes(),
            $this->rules(),
            $this->messages()
        );

        return $validator->validate();
    }
}
