<?php


namespace App\DesignPatterns\Delegation;


use App\DesignPatterns\PropertyContainer\AbstractPropertyContainer;
use Illuminate\Http\Request;

class AbstractRequest extends AbstractPropertyContainer
{
    /**
     * AbstractRequest constructor.
     * @param array $attributes
     * @throws \Exception
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct(array_merge($attributes, $this->getRequestData()));
    }

    /**
     * @return string
     */
    public function url(): string
    {
        return $this->resolveReferer() . $this->resolveRequestUri();
    }

    /**
     * @return array
     */
    public function segments(): array
    {
        return explode('/', $this->resolveRequestUri());
    }

    /**
     * @param int $segment
     * @return string|null
     */
    public function segment(int $segment): ?string
    {
        return $this->segments()[$segment] ?? null;
    }

    /**
     * @return string
     */
    private function resolveReferer(): string
    {
        $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";

        return $protocol . $_SERVER['HTTP_HOST'];
    }

    /**
     * @return string
     */
    private function resolveRequestUri(): string
    {
        return $_SERVER['REQUEST_URI'];
    }

    /**
     * @return array
     * @throws \Exception
     */
    private function getRequestData(): array
    {
        switch ($_SERVER['REQUEST_METHOD']){
            case 'GET':
                return $_GET;
            case 'POST':
            case 'PUT':
                return $_POST;
            default:
                throw new \Exception('Unresolved Request Method');
        }
    }
}
