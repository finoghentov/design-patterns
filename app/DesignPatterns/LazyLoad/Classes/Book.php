<?php


namespace App\DesignPatterns\LazyLoad\Classes;


class Book extends DefaultModel
{
    /**
     * @return Author
     */
    public function author(): Author
    {
        return !isset($this->relations[__FUNCTION__]) ? $this->relations[__FUNCTION__] = new Author(['name' => 'Author Name']) : $this->relations[__FUNCTION__];
    }
}
