**Ленивая загрузка (LazyLoad)**

Основная идея этого шаблона заключается в том, чтобы при инициализации объекта отложить инициализацию связанных объектов или выполнение каких-то трудоёмких задач или вычислений для экономии памяти.

Примером можно привести обычное отношение, например один ко многим. Не всегда получая объект Blog нам нужно сразу инициализировать все его комментарии, возможно комментарии нам вообще не нужны в некоторых ситуациях. 
Здесь нам на помощь приходит ленивая загрузка. Мы получим нужные вычесления или связанный объект только тогда, когда мы к ним обратимся
