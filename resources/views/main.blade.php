@extends('layouts.app')
@section('content')
    <div class="wrapper d-flex align-items-stretch">
        @include('layouts.sidebar')

        <div id="content" class="p-4 p-md-5">
            <h2 class="mb-4">{{$title ?? ''}}</h2>
            <p>{!! $description ?? '' !!}</p>
        </div>
    </div>

@stop
