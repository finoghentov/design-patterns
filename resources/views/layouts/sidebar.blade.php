<nav id="sidebar">
    <div class="p-4 pt-5">
        <ul class="list-unstyled components mb-5">
            <li @if(\Illuminate\Support\Facades\Route::currentRouteName() == 'home') class="active" @endif>
                <a href="{{route('home')}}">Main</a>
            </li>
            @foreach($links ?? [] as $link)
                @php
                    $children = $link['children'] ?? [];

                    $active = array_filter($children, function ($item){
                        return $item['active'];
                    });
                @endphp
                <li @if(isset($link['active']) && $link['active']) class="active" @endif>
                    <a @if(!isset($link['children'])) href="{{$link['route']}}" @endif @if(isset($link['children'])) href="#menu{{$loop->iteration}}" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle @if(!empty($active)) collapsed @endif" @endif>{{$link['name']}}</a>
                    @if(isset($link['children']))
                        <ul class="collapse list-unstyled @if(!empty($active)) show @endif" id="menu{{$loop->iteration}}">
                            @foreach($link['children'] as $link)
                            <li @if($link['active']) class="active" @endif>
                                <a href="{{$link['route']}}">{{$link['name']}}</a>
                            </li>
                            @endforeach
                        </ul>
                    @endif
                </li>
            @endforeach
        </ul>
    </div>
</nav>
